import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {setTimeout} from "timers";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  projectForm: FormGroup;

  statuses:string[] = ['Stable', 'Critical', 'Finished'];

  ngOnInit() {
      this.projectForm = new FormGroup({
          'projectName': new FormControl(null, [Validators.required], [this.asyncNameForbiden]),
          'email': new FormControl(null, [Validators.required, Validators.email]),
          'status': new FormControl(this.statuses[0])
      });

  }

  nameForbiden(control: FormControl): {[s: string]: boolean} {
    if (control.value === 'test') {
      return {'nameIsForbidden': true};
    } else {
      return null;
    }
  }

  asyncNameForbiden(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test') {
          resolve({nameIsForbidden: true});
        } else {
          resolve(null);
        }
      } ,1500);
    });
    return promise;
  }

  onSubmit() {
    console.log(this.projectForm);
  }
}
